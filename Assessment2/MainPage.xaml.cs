﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        

        public MainPage()
        {
            this.InitializeComponent();
            populateComboBox();
        }

        // Populate combobox items when the program is run rather than hard coded
        private void populateComboBox()
        {
            Subject.Items.Add("Non-urgent");
            Subject.Items.Add("Urgent");
            Subject.Items.Add("Very-urgent");

        }

        private async void Submit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var message = $"To: {Department.Text} \n" +
                                $"From: {From.Text}\n" +
                                $"Subject: {Subject.SelectedValue.ToString()}\n" +
                                $"Message: {Message.Text}\n";


                var dialog = new MessageDialog(message, "Send?");

                //Add the buttons to the dialog box and set ID's for them
                dialog.Commands.Add(new UICommand($"Send to {Department.Text}") { Id = 0 });
                dialog.Commands.Add(new UICommand("Cancel") { Id = 1 });

                //Phones don't like three button message dialogs
                if (Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily != "Windows.Mobile")
                {
                    dialog.Commands.Add(new UICommand("Reset") { Id = 2 });
                }

                dialog.DefaultCommandIndex = 0;
                dialog.CancelCommandIndex = 1;

                var result = await dialog.ShowAsync();

                //Determine which button on the dialog box is pressed.
                //The cancel button (Id = 1) is not included as it's default action is to close the dialog box
                //Which is what we want anyway
                if ( Convert.ToInt32(result.Id) == 0 )
                {
                    var sent = new MessageDialog($"Message sent to: {Department.Text}");
                    await sent.ShowAsync();

                    resetForm();
                }
                else if ( Convert.ToInt32(result.Id) == 2)
                {
                    resetForm();
                }

            } catch (Exception ex)
            {
                //Display a message box informing user that the other messagebox didn't display correctly.
                //Messageception
                var dialog = new MessageDialog(ex.Message);
                await dialog.ShowAsync();
            }
        }

        private void resetForm()
        {
            //Reset all form fields
            Department.Text = "";
            From.Text = "";
            Subject.SelectedIndex = -1;
            Message.Text = "";
        }

    }
}
